module tests.api.queue;

import libmpcd.client;

unittest {
    auto client = Client.create("127.0.0.1", 12345);

    client.queue.add("Sample Dir1/sample1.ogg");
    client.queue.add("Sample Dir2/sample2.ogg");

    auto first_queue = client.queue.list();
    assert(first_queue.length == 2);

    assert(first_queue[0].uri() == "Sample Dir1/sample1.ogg");
    assert(first_queue[0].artist() == "Sample Artist");
    assert(first_queue[0].title() == "Sample Track");
    assert(first_queue[0].trackNumber() == "1");

    assert(first_queue[1].uri() == "Sample Dir2/sample2.ogg");
    assert(first_queue[1].artist() == "Sample2 Artist2");
    assert(first_queue[1].title() == "Sample2 Track2");
    assert(first_queue[1].trackNumber() == "2");

    client.queue.add("Sample Dir2/sample3.ogg");
    client.queue.add("Sample Dir2/sample3.ogg");

    auto second_queue = client.queue.list();
    assert(second_queue.length == 4);

    assert(second_queue[2].uri() == "Sample Dir2/sample3.ogg");
    assert(second_queue[2].artist() == "Sample3 Artist3");
    assert(second_queue[2].title() == "Sample3 Track3");
    assert(second_queue[2].trackNumber() == "3");

    assert(second_queue[3].uri() == "Sample Dir2/sample3.ogg");
    assert(second_queue[3].artist() == "Sample3 Artist3");
    assert(second_queue[3].title() == "Sample3 Track3");
    assert(second_queue[3].trackNumber() == "3");

    client.queue.remove(0);
    assert(client.queue.list().length == 3);

    client.queue.remove(0, 2);
    assert(client.queue.list().length == 1);

    client.queue.remove();
    assert(client.queue.list().length == 0);
}
