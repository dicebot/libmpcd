import blackbox.tester;

void main() {
    import std.path : getcwd;
    import std.stdio;
    import std.process;

    TestConfiguration config;

    config.sandbox_dir = "sandbox";
    config.startup_delay = dur!"msecs"(500);
    config.tested_app = ["mpd", "--no-daemon", "./mpd.conf"];

    config.preparations = {

        executeShell("mkdir ./music");
        executeShell("mkdir ./playlists");

        auto cwd = getcwd();

        auto config = File("./mpd.conf", "w");
        config.writeln(`db_file "` ~ cwd ~ `/database"`);
        config.writeln(`pid_file "` ~ cwd ~ `/database"`);
        config.writeln(`log_file "` ~ cwd ~ `/mpd.log"`);
        config.writeln(`music_directory "` ~ cwd ~ `/music"`);
        config.writeln(`port "12345"`);
        config.writeln(`audio_output {`);
        config.writeln(`type "pipe"`);
        config.writeln(`name "null pipe"`);
        config.writeln(`command "/dev/null"`);
        config.writeln(`}`);
        config.close();

        executeShell("cp -r ../tests/samples/music/* ./music/");
        executeShell("cp ../tests/samples/database ./");
    };

    runTests(config);
}
