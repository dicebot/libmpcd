module tests.api.database;

import libmpcd.client;
import libmpcd.entities;
import std.algorithm;

unittest {
    auto client = Client.create("127.0.0.1", 12345, 5000);

    auto root_ls = client.database.get("/");
    assert(root_ls.length == 3);
    auto root_dirs = root_ls.map!(entity => entity.get!Directory);
    assert(root_dirs[0].path() == "Playlist_samples");
    assert(root_dirs[1].path() == "Sample Dir1");
    assert(root_dirs[2].path() == "Sample Dir2");

    auto dir1 = client.database.get("Sample Dir1");
    assert(dir1.length == 1);
    assert(dir1[0].get!Song.uri() == "Sample Dir1/sample1.ogg");
    assert(dir1[0].get!Song.artist() == "Sample Artist");
    assert(dir1[0].get!Song.title() == "Sample Track");
    assert(dir1[0].get!Song.trackNumber() == "1");

    auto dir2 = client.database.get("Sample Dir2");
    assert(dir2.length == 2);
    assert(dir2[0].get!Song.uri() == "Sample Dir2/sample2.ogg");
    assert(dir2[0].get!Song.artist() == "Sample2 Artist2");
    assert(dir2[0].get!Song.title() == "Sample2 Track2");
    assert(dir2[0].get!Song.trackNumber() == "2");

    assert(dir2[1].get!Song.uri() == "Sample Dir2/sample3.ogg");
    assert(dir2[1].get!Song.artist() == "Sample3 Artist3");
    assert(dir2[1].get!Song.title() == "Sample3 Track3");
    assert(dir2[1].get!Song.trackNumber() == "3");
}

unittest {
    auto client = Client.create("127.0.0.1", 12345);
    auto playlists = client.database.get("Playlist_samples");
    assert(playlists[0].get!Playlist.path() == "Playlist_samples/Playlist Sample1.m3u");
}
