module tests.api.player;

import libmpcd.client;
import libmpcd.player;
import libmpcd.core.exception;
import std.exception;

unittest {
    auto client = Client.create("127.0.0.1", 12345);

    client.queue.add("Sample Dir1/sample1.ogg");

    client.player.play();
    assert(client.player.state() == PlaybackState.Play);

    auto current = client.player.currentSong();
    assert(current.uri() == "Sample Dir1/sample1.ogg");
    assert(current.artist() == "Sample Artist");
    assert(current.title() == "Sample Track");
    assert(current.trackNumber() == "1");

    client.queue.add("Sample Dir2/sample2.ogg");

    client.player.togglePause();
    assert(client.player.state() == PlaybackState.Pause);
    client.player.togglePause();
    assert(client.player.state() == PlaybackState.Play);
    client.player.stop();
    assert(client.player.state() == PlaybackState.Stop);

    assertThrown!APIException(client.player.next());
    client.player.play();
    client.player.next();

    auto next = client.player.currentSong();

    assert(next.uri() == "Sample Dir2/sample2.ogg");
    assert(next.artist() == "Sample2 Artist2");
    assert(next.title() == "Sample2 Track2");
    assert(next.trackNumber() == "2");
}
