/+ dub.sdl:
    name         "example-status"
    targetType   "executable"
    dependency   "libmpcd"   path=".."
 +/

import libmpcd.client;
import libmpcd.player;
import libmpcd.core.exception;
import std.stdio;

void main() {
    auto client = Client.create("127.0.0.1", 12345);

    try {
        auto song = client.player.currentSong();
        writeln("Artist: ", song.artist);
        writeln("Title: ", song.title);
        writeln(client.player.state);
    }
    catch (APIException) {
    }
}
