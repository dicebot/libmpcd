module libmpcd.database;

/**
 * A structure that represents a database.
 */
struct Database {
    import libmpcd.core.connection;
    import libmpcd.entities;
    import libmpcd.c.database;
    import libmpcd.c.entity;

    private Connection connection;

    /**
     * Params:
     *      path = a path to an entity
     *
     * Returns:
     *      An array of received entities.
     */
    Entity[] get(const(char[]) path) {
        import std.string : toStringz;

        typeof(return) result;

        auto success = mpd_send_list_meta(this.connection.context, toStringz(path));
        if (!success)
            this.connection.expectError();

        for (;;) {
            auto current = mpd_recv_entity(this.connection.context);

            if (current is null) {
                this.connection.checkError();
                break; // end of entity stream
            }
            else {
                result ~= wrapEntity(current);
            }
        }

        return result;
    }
}
