module libmpcd.entities;

import libmpcd.c.entity;
import libmpcd.c.opaque_types;
import std.variant;

public import libmpcd.entities.song;
public import libmpcd.entities.directory;
public import libmpcd.entities.playlist;

/**
 * The type of a mpd_entity object.
 */
enum MPDEntity
{
    /**
     * The type of the entity received from MPD is not implemented
     * in this version of libmpdclient
     */
    Unknown   = mpd_entity_type.MPD_ENTITY_TYPE_UNKNOWN,
    /**
     * A directory containing more entities
     */
    Directory = mpd_entity_type.MPD_ENTITY_TYPE_DIRECTORY,
    /**
     * A song file which can be added to the playlist
     */
    Song      = mpd_entity_type.MPD_ENTITY_TYPE_SONG,
    /**
     * A stored playlist
     */
    Playlist  = mpd_entity_type.MPD_ENTITY_TYPE_PLAYLIST,
}

alias Entity = Algebraic!(Directory, Song, Playlist);

/**
 * Params:
 *      raw = a pointer to a mpd_entity object
 *
 * Returns:
 *      a certain  entity.
 */
Entity wrapEntity(mpd_entity* raw)
{
    auto type = cast(MPDEntity) mpd_entity_get_type(raw);

    final switch (type)
    {
        case MPDEntity.Unknown:
            return Entity.init;
        case MPDEntity.Song:
            return Entity(Song(mpd_entity_get_song(raw)));
        case MPDEntity.Directory:
            return Entity(Directory(mpd_entity_get_directory(raw)));
        case MPDEntity.Playlist:
            return Entity(Playlist(mpd_entity_get_playlist(raw)));
    }
}
