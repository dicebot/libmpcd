/* libmpdclient
   (c) 2003-2015 The Music Player Daemon Project
   This project's homepage is: http://www.musicpd.org

   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions
   are met:

   - Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.

   - Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE FOUNDATION OR
   CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
    Defines all opaque struct types used in all other modules to prevent
    symbol clash conflicts on D side of things (which isn't a problem in
    C because there are no modules and everything is one namespace).
 **/

module libmpcd.c.opaque_types;

extern (C):

/**
 * \struct mpd_async
 *
 * This opaque object represents an asynchronous connection to a MPD
 * server.  Call mpd_async_new() to create a new instance.
 */
struct mpd_async;

/**
 * \struct mpd_connection
 *
 * This opaque object represents a connection to a MPD server.  Call
 * mpd_connection_new() to create a new instance.  To free an
 * instance, call mpd_connection_free().
 *
 * Error handling: most functions return a "bool" indicating success
 * or failure.  In this case, you may query the nature of the error
 * with the functions mpd_connection_get_error(),
 * mpd_connection_get_error_message(),
 * mpd_connection_get_server_error().
 *
 * Some errors can be cleared by calling mpd_connection_clear_error(),
 * like #MPD_ERROR_SERVER, #MPD_ERROR_ARGUMENT.  Most others are
 * fatal, and cannot be recovered, like #MPD_ERROR_CLOSED -
 * mpd_connection_clear_error() returns false.
 *
 * Some functions like mpd_recv_pair() cannot differentiate between
 * "end of response" and "error".  If this function returns NULL, you
 * have to check mpd_connection_get_error().
 */
struct mpd_connection;

/**
 * \struct mpd_directory
 *
 * An opaque directory object.  This is a container for more songs,
 * directories or playlists.
 */
struct mpd_directory;

/**
 * \struct mpd_entity
 *
 * An "entity" is an object returned by commands like "lsinfo".  It is
 * an object wrapping all possible entity types.
 */
struct mpd_entity;

/**
 * \struct mpd_message
 */
struct mpd_message;

/**
 * \struct mpd_output
 *
 * This type represents an audio output device on the MPD server.
 */
struct mpd_output;

/**
 * \struct mpd_playlist
 *
 * An opaque representation for a playlist stored in MPD's
 * playlist directory.  Use the functions provided by this header to
 * access the object's attributes.
 */
struct mpd_playlist;

/**
 * \struct mpd_parser
 *
 * This opaque object is a low-level parser for the MPD protocol.  You
 * feed it with input lines, and it provides parsed representations.
 */
struct mpd_parser;

/**
 * \struct mpd_settings
 *
 * An object which describes configurable connection settings.
 */
struct mpd_settings;

/**
 * \struct mpd_song
 *
 * An opaque representation for a song in MPD's database or playlist.
 * Use the functions provided by this header to access the object's
 * attributes.
 */
struct mpd_song;

/**
 * \struct mpd_stats
 *
 * An opaque object representing MPD's response to the "stats"
 * command.  To release this object, call mpd_stats_free().
 */
struct mpd_stats;

/**
 * \struct mpd_status
 *
 * Holds information about MPD's status.
 */
struct mpd_status;
