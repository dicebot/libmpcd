/* libmpdclient
   (c) 2003-2015 The Music Player Daemon Project
   This project's homepage is: http://www.musicpd.org

   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions
   are met:

   - Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.

   - Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.

   - Neither the name of the Music Player Daemon nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE FOUNDATION OR
   CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/*! \file
 * \brief MPD client library
 *
 * Do not include this header directly.  Use mpd/client.h instead.
 */

module libmpcd.c.song;

import core.stdc.time;

import libmpcd.c.opaque_types;
import libmpcd.c.pair;
import libmpcd.c.tag;

alias mpd_song = libmpcd.c.opaque_types.mpd_song;

extern (C):

/**
 * Free memory allocated by the #mpd_song object.
 */
void mpd_song_free (mpd_song* song);

/**
 * Duplicates the specified #mpd_song object.
 *
 * @returns the copy, or NULL if out of memory
 */
mpd_song* mpd_song_dup (const(mpd_song)* song);

/**
 * Returns the URI of the song.  This is either a path relative to the
 * MPD music directory (without leading slash), or an URL with a
 * scheme, e.g. a HTTP URL for a radio stream.
 */
const(char)* mpd_song_get_uri (const(mpd_song)* song);

/**
 * Queries a tag value.
 *
 * @param song the song object
 * @param type the tag type
 * @param idx pass 0 to get the first value for this tag type.  This
 * argument may be used to iterate all values, until this function
 * returns NULL
 * @return the tag value, or NULL if this tag type (or this index)
 * does not exist
 */
const(char)* mpd_song_get_tag (
    const(mpd_song)* song,
    mpd_tag_type type,
    uint idx);

/**
 * Returns the duration of this song in seconds.  0 means the duration
 * is unknown.
 */
uint mpd_song_get_duration (const(mpd_song)* song);

/**
 * Returns the duration of this song in milliseconds.  0 means the
 * duration is unknown.
 */
uint mpd_song_get_duration_ms (const(mpd_song)* song);

/**
 * Returns the start of the virtual song within the physical file in
 * seconds.
 */
uint mpd_song_get_start (const(mpd_song)* song);

/**
 * Returns the end of the virtual song within the physical file in
 * seconds.  Zero means that the physical song file is played to the
 * end.
 */
uint mpd_song_get_end (const(mpd_song)* song);

/**
 * @return the POSIX UTC time stamp of the last modification, or 0 if
 * that is unknown
 */
time_t mpd_song_get_last_modified (const(mpd_song)* song);

/**
 * Sets the position within the queue.  This value is not used for
 * songs which are not in the queue.
 *
 * This function is useful when applying the values returned by
 * mpd_recv_queue_change_brief().
 */
void mpd_song_set_pos (mpd_song* song, uint pos);

/**
 * Returns the position of this song in the queue.  The value is
 * undefined if you did not obtain this song from the queue.
 */
uint mpd_song_get_pos (const(mpd_song)* song);

/**
 * Returns the id of this song in the playlist.  The value is
 * undefined if you did not obtain this song from the queue.
 */
uint mpd_song_get_id (const(mpd_song)* song);

/**
 * Returns the priority of this song in the playlist.  The value is
 * undefined if you did not obtain this song from the queue.
 */
uint mpd_song_get_prio (const(mpd_song)* song);

/**
 * Begins parsing a new song.
 *
 * @param pair the first pair in this song (name must be "file")
 * @return the new #mpd_entity object, or NULL on error (out of
 * memory, or pair name is not "file")
 */
mpd_song* mpd_song_begin (const(mpd_pair)* pair);

/**
 * Parses the pair, adding its information to the specified
 * #mpd_song object.
 *
 * @return true if the pair was parsed and added to the song (or if
 * the pair was not understood and ignored), false if this pair is the
 * beginning of the next song
 */
bool mpd_song_feed (mpd_song* song, const(mpd_pair)* pair);

/**
 * Receives the next song from the MPD server.
 *
 * @return a #mpd_song object, or NULL on error or if the song list is
 * finished
 */
mpd_song* mpd_recv_song (mpd_connection* connection);

