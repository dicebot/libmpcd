/* libmpdclient
   (c) 2003-2015 The Music Player Daemon Project
   This project's homepage is: http://www.musicpd.org

   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions
   are met:

   - Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.

   - Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE FOUNDATION OR
   CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/*! \file
 * \brief MPD client library
 *
 * Controlling playback.
 *
 * Do not include this header directly.  Use mpd/client.h instead.
 */

module libmpcd.c.player;

import libmpcd.c.opaque_types;

extern (C):

/**
 * Fetches the currently selected song (the song referenced by
 * status->song and status->songid).
 */
bool mpd_send_current_song (mpd_connection* connection);

/**
 * Shortcut for mpd_send_currentsong() and mpd_recv_song().
 *
 * @param connection the connection to MPD
 * @return the current song, or NULL on error or if there is no
 * current song
 */
mpd_song* mpd_run_current_song (mpd_connection* connection);

/**
 * Starts playing the current song from the beginning.
 *
 * @param connection the connection to MPD
 */
bool mpd_send_play (mpd_connection* connection);

bool mpd_run_play (mpd_connection* connection);

/**
 * Starts playing the specified song from the beginning.
 *
 * @param song_pos the position of the song in the queue
 * @param connection the connection to MPD
 * @return true on success, false on error
 */
bool mpd_send_play_pos (mpd_connection* connection, uint song_pos);

bool mpd_run_play_pos (mpd_connection* connection, uint song_pos);

/**
 * Starts playing the specified song from the beginning.
 *
 * @param connection the connection to MPD
 * @param id the id of the song
 * @return true on success, false on error
 */
bool mpd_send_play_id (mpd_connection* connection, uint id);

bool mpd_run_play_id (mpd_connection* connection, uint song_id);

bool mpd_send_stop (mpd_connection* connection);

bool mpd_run_stop (mpd_connection* connection);

/**
 * Toggles the pause mode by sending "pause" without arguments.
 *
 * @param connection the connection to MPD
 */
bool mpd_send_toggle_pause (mpd_connection* connection);

bool mpd_run_toggle_pause (mpd_connection* connection);

bool mpd_send_pause (mpd_connection* connection, bool mode);

bool mpd_run_pause (mpd_connection* connection, bool mode);

bool mpd_send_next (mpd_connection* connection);

bool mpd_run_next (mpd_connection* connection);

bool mpd_send_previous (mpd_connection* connection);

bool mpd_run_previous (mpd_connection* connection);

/**
 * Seeks the specified song.
 *
 * @param connection the connection to MPD
 * @param song_pos the position of the song in the queue
 * @param t the position within the song, in seconds
 * @return true on success, false on error
 */
bool mpd_send_seek_pos (mpd_connection* connection, uint song_pos, uint t);

bool mpd_run_seek_pos (mpd_connection* connection, uint song_pos, uint t);

/**
 * Seeks the specified song.
 *
 * @param connection the connection to MPD
 * @param id the id of the song
 * @param t the position within the song, in seconds
 * @return true on success, false on error
 */
bool mpd_send_seek_id (mpd_connection* connection, uint id, uint t);

bool mpd_run_seek_id (mpd_connection* connection, uint song_id, uint t);

bool mpd_send_repeat (mpd_connection* connection, bool mode);

bool mpd_run_repeat (mpd_connection* connection, bool mode);

bool mpd_send_random (mpd_connection* connection, bool mode);

bool mpd_run_random (mpd_connection* connection, bool mode);

bool mpd_send_single (mpd_connection* connection, bool mode);

bool mpd_run_single (mpd_connection* connection, bool mode);

bool mpd_send_consume (mpd_connection* connection, bool mode);

bool mpd_run_consume (mpd_connection* connection, bool mode);

bool mpd_send_crossfade (mpd_connection* connection, uint seconds);

bool mpd_run_crossfade (mpd_connection* connection, uint seconds);

bool mpd_send_mixrampdb (mpd_connection* connection, float db);

bool mpd_run_mixrampdb (mpd_connection* connection, float db);

bool mpd_send_mixrampdelay (mpd_connection* connection, float seconds);

bool mpd_run_mixrampdelay (mpd_connection* connection, float seconds);

bool mpd_send_clearerror (mpd_connection* connection);

bool mpd_run_clearerror (mpd_connection* connection);

