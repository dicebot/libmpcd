/* libmpdclient
   (c) 2003-2015 The Music Player Daemon Project
   This project's homepage is: http://www.musicpd.org

   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions
   are met:

   - Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.

   - Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE FOUNDATION OR
   CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/*! \file
 * \brief MPD client library
 *
 * This is a client library for the Music Player Daemon, written in C.
 *
 * You can choose one of several APIs, depending on your requirements:
 *
 * - struct mpd_async: a very low-level asynchronous API which knows
 *   the protocol syntax, but no specific commands
 *
 * - struct mpd_connection: a basic synchronous API which knows all
 *   MPD commands and parses all responses
 *
 * \author Max Kellermann (max@duempel.org)
 */

module libmpcd.c.client;

extern (C):

public import libmpcd.c.opaque_types;
public import libmpcd.c.audio_format;
public import libmpcd.c.capabilities;
public import libmpcd.c.connection;
public import libmpcd.c.database;
public import libmpcd.c.directory;
public import libmpcd.c.entity;
public import libmpcd.c.idle;
public import libmpcd.c.list;
public import libmpcd.c.message;
public import libmpcd.c.mixer;
public import libmpcd.c.output;
public import libmpcd.c.pair;
public import libmpcd.c.password;
public import libmpcd.c.player;
public import libmpcd.c.playlist;
public import libmpcd.c.queue;
public import libmpcd.c.recv;
public import libmpcd.c.response;
public import libmpcd.c.search;
public import libmpcd.c.send;
public import libmpcd.c.settings;
public import libmpcd.c.song;
public import libmpcd.c.stats;
public import libmpcd.c.status;
public import libmpcd.c.sticker;
public import libmpcd.c.version_;
